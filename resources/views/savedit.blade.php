
@extends('layout.layout')
@section('content')
  <div class="card border-primary mb-3">
    <div class="justify content-between">
      <div class="card-header">Sav edition</div>

    </div>

    <div class="card-body text-primary">

      <div class="">
        <form action="{{route('sav.update',['sav'=> $sav])}}" method="POST" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container="">
        @csrf
        @method('PATCH')
          <div class="form-group">
            <label for="exampleInputEmail1">Nom clients</label>
            <input type="nom_client" name="nom_client" value="{{$sav->nom_client}}" class="form-control" id="exampleInputEmail1" aria-describedby="name" placeholder="Entree votre name">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Email clients</label>
            <input type="email" name="email_client" value="{{$sav->email_client}}"  class="form-control" id="exampleInputPassword1" placeholder="email cliens">
          </div>
          <div class="form-group">
            <label for="contact">Contact clients</label>
            <input name="contact_client" type="text" value="{{$sav->contact_client}}" class="form-control" id="contact" placeholder="contact">
          </div>
          <div class="form-group">
            <label for="motif">Motif </label>
            <input name="motif" value="{{$sav->motif}}" type="text" class="form-control" id="motif" placeholder="le motif ">
          </div>
          <div class="form-group">
            <label for="designation">Designation</label>
            <input name="designation" value="{{$sav->designation}}" type="text" class="form-control" id="designation" placeholder="designation">
          </div>


        <div class="form-group">
          <label for="qte">quantite</label>
          <input name="qte" value="{{$sav->qte}}" type="number" class="form-control" id="qte" placeholder="qte">
        </div>
      </div>
        <div class="form-group">
          <label for="qte">Nom tech </label>
          <input name="nom_tech" value="{{$sav->nom_tech}}" type="text" class="form-control" id="nom_tech" placeholder="nom technicien">
        </div>

        <div class="form-group">
          <label for="email_tech">Email tech </label>
          <input name="email_tech" value="{{$sav->email_tech}}" type="text" class="form-control" id="email_tech" placeholder="nom technicien">
        </div>


        <div class="form-group">
          <label for="contact_tech">Contact technicien </label>
          <input name="contact_tech"  value="{{$sav->contact_tech}}" type="text" class="form-control" id="contact_tech" placeholder="contact_tech">
        </div>

        <div class="form-group">
          <label for="serie">Numero de serie </label>
          <input value="{{$sav->serie}}" name="serie" type="text" class="form-control" id="serie" placeholder="serie">
        </div>


        <div class="form-group">
          <label for="serie">les Options </label>
          <select name="option" class="form-control" id="option" placeholder="les options">
            <option>choisie</option>

            <option value="garantie">garantie</option>
            <option value="maintenance">maintenance</option>

          </select>
        </div>

        <div class="form-group">
          <label for="serie">La date </label>
          <input value="{{$sav->DateSav}}" name="DateSav" type="date" class="form-control" id="serie" placeholder="date">
        </div>
        <div class="card-footer">

          <div class="col-md-8">
            <div class="btn-group pull-right">
              <button type="submit" class="btn btn-primary">Soumettre</button>
            </div>
            <div class="btn-group pull-left">
              <button type="reset" class="btn btn-warning">Réinitialiser</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@stop
