<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    public  function  index (){
        
        return view('mail');
    }

         public  function send (Request $request){

        $this->validate($request,[

            'objet'=> 'required',
            'mail' => 'required',
            'message' => 'required '


        ]);

        $data = array(
            'objet'  => $request->objet,
            'message' => $request->message
        );

        Mail::to('gnakaleroland@gmail.com')->send(new SendMail($data
        ));

        return back();
    }
}
