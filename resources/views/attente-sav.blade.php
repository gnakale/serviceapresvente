@extends("layout.layout")

@section('content')
    <!doctype html>
<html lang="fr" xmlns:width="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voir un Sav</title>
</head>
<body>

<div class="card">
    <div class="card-header">
        Information
    </div>
    <div class="card-body">

        <table class="table-responsive">

            <div class="box-body">
                <div class="row offset-1">
                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->name}}">
                    </div>

                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->designationctl}}">
                    </div>

                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->seriectl}}">
                    </div>
                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->motifctl}}">
                    </div>

                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->contactctl}}">
                    </div>

                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->emailctl}}">
                    </div>
                    <div class="form-group col-md-5">

                        <label for="" class="control-label">Nom client </label>
                        <input type="text" disabled class="form-control" value="{{$res->dateAchat}}">
                    </div>





                    <div class="row">


                    </div>
                </div>
            </div>
    </div>
</div>
</table>

</div>
</div>

</body>

@stop
