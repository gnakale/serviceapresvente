@extends("layout.layout")

@section('content')
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voir un Sav</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

</body>
</html>

<div class="row card p-3 m-2">

    @if($errors->any())


    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        @foreach($errors->all() as $error)


        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <li>
            {!!$error!!}
        </li>
        @endforeach
    </div>
    @endif
    <form method="POST" action="reparer.store" >
        @csrf
        <div class="p-2">
            <div class="">
                <label class="control-label">
                  Designation

                </label>
                <input name="Designation"   type="text" class="form-control">

            </div>

            <div class="">
                <label class="control-label">
                    Numero de Serie

                </label>
                <input name="numserie"   type="text" class="form-control">

            </div>
            <div class="">
                <label class="control-label" for="" >
                    Date Reparation
                </label>
                <input name="date" type="date" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Motif  de la panne

                </label>
                <textarea name="Motif" type="text" class="form-control"></textarea>
            </div>

            <div class="">
                <label class="control-label" for="" >
                   Methode de Réparation

                </label>
                <textarea name="Methode" type="text" class="form-control"></textarea>
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Nom client
                </label>
                <input name="Nomclient" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Contact Client

                </label>
                <input name="ContactClient" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                  Nom Technicien

                </label>
                <input name="NomTech" type="text" class="form-control">
            </div>
            <div class="">
                <label class="control-label" for="" >
                    Contact Technicien

                </label>
                <input  name="ContactTech" type="tel" class="form-control ">
            </div>

        <input type="submit"class="btn btn-primary py-2 m-2">
    </form>
</div>
</div>

@stop
