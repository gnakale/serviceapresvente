<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','IndexController@index');




Route::get('/contact','ContactController@contact');




Route::get('/service','ServiceController@service');




Route::get('/inscription/create','InscriptionController@create')->name('inscription');




Route::post('/inscription','InscriptionController@store')->name('inscriptions');




Route::get('/confirmation/{user}/{token}/','ConfirmationController@store')->name('confirmation.store');



//Route::get('/confirmation/{user}/{token}','ConfirmationController@store')->name('confirmation.store');


Route::get('/sav.create', 'SavController@create')->name('sav.create');


Route::post('/sav.store', 'SavController@store')->name('sav.store');


Route::get('/sav.index','SavController@index')->name('sav.index');

Route::get('sav.show/{sav}','SavController@show')->name('sav.show');


Route::get('sav.edit/{sav}/edit','SavController@edit')->name('sav.edit');

Route::post('/sav.destroy/{id}/','SavController@destroy')->name('sav.destroy');



Route::patch('sav.update/{sav}','SavController@update')->name('sav.update');




//Route::get('/admin','AdminController@show')->name('admin');




Route::get('monservice/list','listControler@list');



Route::get('/savCreate',' savController@create')->name('savCreate');



Route::group(['namespace'=>'Auth'],function (){


    Route::get('login','LoginsController@create')->name('login');
    Route::post('login','LoginsController@store');

    Route::get('logout','LoginsController@destroy')->name('logout');



});

// Deconnexion des utilisateur


Route::get('/attente.index','AttenteController@index')->name('attente.index');
Route::get('/attente.show/{res}','AttenteController@show')->name('attente.show');




// passse un sav

Route::get('reserve','ReserveController@show')->name('reserve.sav');


Route::get('reserve.mesav','ReserveController@mesav')->name('reserve.mesav');


Route::post('reserve','ReserveController@store');




Route::get('/admin', 'HomeController@index')->name('admin');


//
//
//Route::group(['namespace'=>'vente'],function (){
//
//
//    Route::post('vente','VenteController@store')->name('vente.store');
//
//});




Route::get('/vente.index','VenteController@index')->name('vente.index');
Route::get('vente.show/{qs}/','VenteController@show')->name('Vente.show');


Route::get('MessageSoumie','MessageSoumieController@index')->name('MessageSoumie');

Route::get('MessageMail','MessageSoumieController@mail')->name('MessageMail');



//Auth::routes();



Route::get('personne.index','PersonneController@index')->name('personne.index');



Route::get('personne/{personne}/','PersonneController@show')->name('personne-view');


//génération de facture


Route::get('facture.index','FactureController@index')->name('facture.index');




Route::get('/facture.print/{id}','FactureController@facture')->name('facture.print');


//contactter nous


Route::post('/contacter','ContactController@store');

Route::get('messagerie.index','ContactController@index')->name('messagerie.index');




Route::get('messagerie/{contacts}/','ContactController@show')->name('messagerie.show');

//option messagerie

Route::get('reparer.index','ReparationController@index')->name('reparer.index');



Route::get('reparer.create','ReparationController@create')->name('reparer.create');


Route::post('reparer.store','ReparationController@store')->name('reparer.store');


Route::get('reparer.show/{reparer}','ReparationController@show')->name('reparer.show');
//edition de profile



Route::put('user');
//

Route::get('garte.index','MachineController@index')->name('garte.index');

Route::get('gate.create','MachineController@create')->name('gate.create');


Route::post('gate.store','MachineController@store')->name('gate.store');



Route::get('contacter',function(){

    return view('contacter');
});


// mission

//profile Controller
// Route::get('profile.edit/{profile}/edit','ProfileController@edit')->name('profile.edit');
//
// Route::patch('profile.update/{profile}','ProfileController@update')->name('profile.update');



//pour allez a route service

Route::get('services',function(){

    return view('services');
});



// creation de la vue smtp cette vue permetra d'envoyer message

Route::get('mail.index','SendEmailController@index')->name('mail.index');
Route::post('send','SendEmailController@send');
