<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('savs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_client');
            $table->string('email_client');
            $table->string('contact_client');
            $table->text('motif');
            $table->text('designation');
            $table->string('qte');
            $table->string('nom_tech');
            $table->string('email_tech');
            $table->string('contact_tech');
            $table->string('serie');

            $table->string('option');
            $table->date('DateSav');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sav');
    }
}
