@extends("layout.layout")

@section('content')
   <!doctype html>
   <html lang="fr">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Voir un Sav</title>
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   </head>
   <body>
      <script type="text/javascript">
      // Dynamically add-on fields

      $(function() {
         // Remove button click
         $(document).on(
            'click',
            '[data-role="appendRow"] > .form-inline [data-role="remove"]',
            function(e) {
               e.preventDefault();
               $(this).closest('.form-row').remove();
            }
         );
         // Add button click
         $(document).on(
            'click',
            '[data-role="appendRow"] > .form-row [data-role="add"]',
            function(e) {
               e.preventDefault();
               var container = $(this).closest('[data-role="appendRow"]');
               new_field_group = container.children().filter('.form-row:first-child').clone();
               new_field_group.find('label').html('Upload Document'); new_field_group.find('input').each(function(){
                  $(this).val('');
               });
               container.append(new_field_group);
            }
         );
      });


      // file upload

      $(document).on('change', '.file-upload', function(){
         var i = $(this).prev('label').clone();
         var file = this.files[0].name;
         $(this).prev('label').text(file);
      });

      </script>

      <style>
      /*

      @import url('https://fonts.googleapis.com/css?family=Open+Sans');
      font-family: 'Open Sans', sans-serif;

      */

      @import url('https://fonts.googleapis.com/css?family=Open+Sans');

      html {

         -ms-text-size-adjust: 100%;
         -webkit-text-size-adjust: 100%;
      }

      body {
         overflow-x:hidden;
         font-family: 'Open Sans', sans-serif;
         position:relative;
      }
      .custom-upload { margin-top:20px;}
      section {
         padding: 60px 0;
         overflow-y: auto !important;
      }

      section .section-title {
         text-align: center;
         color: #007b5e;
         margin-bottom: 50px;
         text-transform: uppercase;
      }
      .btn {margin-right:10px;}
      a {
         -webkit-transition: all 0.5s;
         -moz-transition: all 0.5s;
         transition: all 0.5s;
         font-family: 'Open Sans', sans-serif;
         color: #000;
      }
      a:hover {
         -webkit-transition: all 0.5s;
         -moz-transition: all 0.5s;
         transition: all 0.5s;
         color: #000;
         text-decoration: none;
      }

      h1,h2,h3,h4,h5{
         font-family: 'Open Sans', sans-serif;
      }
      p, ul,li{
         font-family: 'Open Sans', sans-serif;
         font-size: 14px;
         line-height: 25px;
      }
      .form-inline .form-control.custom-file-upload {
         border: 1px solid #ccc;

         padding: 6px 12px;
         cursor: pointer;
         white-space: nowrap;
         overflow: hidden;
         text-overflow: ellipsis;
         width: 100%;
      }

      .wrap-input-container {
         display: inline-block;
         position: relative;
         overflow: hidden;
      }
      .wrap-input-container input {
         position: absolute;
         font-size: 400px;
         opacity: 0;
         z-index: 1;
         top: 0;
         left: 0;
      }

   </style>

   <div class="row card p-3 m-2">

      @if($errors->any())


         <div class="alert alert-warning alert-dismissible fade show" role="alert">
            @foreach($errors->all() as $error)


               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               <li>
                  {!!$error!!}
               </li>
            @endforeach
         </div>
      @endif
      <form method="POST" action="sav.store" >
         @csrf
         <div class="row">
            <div class="col-md-4">
               <label class="control-label">
                  Nom Clients

               </label>
               <input name="nom_client"   type="text" class="form-control">

            </div>

            <div class="col-md-4">
               <label class="control-label" for="" >
                  Email Clients

               </label>
               <input name="email_client" type="email" class="form-control">
            </div>
            <div class="col-md-4">
               <label class="control-label" for="" >
                  Contact Client

               </label>
               <input name="contact_client" type="number" class="form-control">
            </div>
            <div class="col-md-4">
               <label class="control-label" for="" >
                  Nom Technicien

               </label>
               <input name="nom_tech" type="text" class="form-control">
            </div>
            <div class="col-md-4">
               <label class="control-label" for="" >
                  Email Technicien
               </label>
               <input name="email_tech" type="text" class="form-control">
            </div>
            <div class="col-md-4">
               <label class="control-label" for="" >
                  Contact Technicien

               </label>
               <input name="contact_tech" type="text" class="form-control">
            </div>

            <div class="container">

               <div class="form-group row custom-upload"></div>



         <input type="submit"class="btn btn-primary btn-block p-2 m-2">
      </form>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){

      var count = 1;
         dynamyc(count);
      function dynamyc(number)
      {
         var html = '<div>';

         html += '<div> <input type="text" name="motif[]" class="form-control" /> </div>';
         html += '<div> <input type="text" name="serie[]" class="form-control" /> </div>';
         html += '<div> <input type="text" name="designation[]" class="form-control" /> </div>';
         if(number >1){
            html += '<div><button type="button" name="remove" id="remove" class="btn btn-danger">Remove</div>';
            $('div').append(html);
       }
       else {
          html += '<div><button type="button" name="add" id="add" class="btn btn-sucess">Add</div>';
         $('div').html(html)
       }


      }
      $('#add').click(function(){
         count++;
         dynamyc(count)
      });
      $(document).on('click', '#remove', function(){
         count--;
         dynamyc(count);
      });
      ('#dynamyc').on('submit',function(){
         event.preventDefault();
         $.ajax({
            url: '{{route("sav.store")}}',
            method:'post',
            data:$(this).serialize(),
            dataType:'json',
            beforeSend:function(){
               $('#save').attr('disabled','disabled');
            },
            success:function(data){
               if(data.error){
                  var error_html = '';
                  for(var count = 0 ; count < data.error.length;count++)
                  {
                     error_html += '<p>'+data.error[count]+'</p>';

                  }
                  $('#result').html('<div> class="alert alert-danger">'+error_html+'</div>');
               }
               else {
                  dynamyc(1);
                  $('#result').html('<div class="alert alert-sucess">'+data.success+'<div>');

               }
               $('#sav').attr('disabled',false);
            }
         })
      });
   })
</script>
@stop
