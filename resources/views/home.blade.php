<!DOCTYPE html>
	<html lang="fr">
		<head>
			<meta charset="utf-8">
			<title>SAV</title>
			<link rel="stylesheet" type="text/css" href="Styles/CodeCSS.css">
			<link rel="stylesheet" href="Styles/responsivite.css"> 
			<script type="text/javascript" src="Styles/jQuery.js"></script>
		</head>
		<body>

			<!--   L entete du SAV  -->

			<header class="header">	
				<div class="contMenu" id="contMenu">
					<div class="logo">
						<img class="image" src="Images/mt-logo.png">
						<p class="Mediatec">MediaTec SARL</p>
					</div>

					<!--   Le Menu et L entete du SAV    -->
					<div class="menu">
						<ul>
							<li class="activemenu"><a href="/">ACCUEIL</a></li>
							<li><a href="services">NOS SERVICES</a></li>
							<li><a href="contacter">CONTACTER</a></li>
						</ul>
					</div>
					<div class="SAVconnecte">
						<a href="inscription/create"><button class="sInscrire">S'INSCRIRE AU SAV</button></a>
						<a href="login"><button class="seConnecter">CONNEXION</button></a>
					</div>
				</div>
				
				<div class="cont">
					<div class="h1"><p>Bienvenue Panel SAV MediaTeC</p></div>
					<div class="h2">
						<p class="h3">Facile d'Utilisation,<br> Fiabilité Totale.</p>
						<p class="h4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit ... <br>
							Lorem ipsum dolor sit amet, consectetur adipisicing ...
						</p>
					</div>
					<a href=""><button class="savoir">En savoir plus </button></a>
				</div>
			</header>
			


			<!--   Le corps de page du SAV  -->
			<div class="lecorps">
				<ul>
					<li class="h11"><b>MediaTeC SAV</b></li>
					<li class="h22"><b>Un service de qualité</b></li>
					<li>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum nam dolorem quibusdam et, accusamus dolore consequuntur delectus quidem, nesciunt ullam voluptates, sapiente, placeat! Nostrum maxime repellendus iste optio, consequuntur, magnam id harum odit molestias, enim veniam minima perspiciatis.
						</p>
					</li>
				</ul>
			</div>



			<!--   Le Footer ou pied de page du SAV  -->
			<footer>
				<div class="dive1">
					<div class="logof">
						<img src="Images/mth-logo.png">
						<p class="Mediate">MediaTec SARL</p>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium amet accusamus iure illum quidem saepe nam veritatis porro, ducimus aperiam delectus. <br><br> Architecto quos similique sit libero officia molestias accusantium ipsa a facilis sunt ab maxime dolorum modi labore? 
						<br><br>
						<b class="CopyRight"style=" font-size: 12px; color: #666">CopyRight 2019 MediaTeC || Solution</b>
					</p>
				</div>


				<div class="dive2">
					<p class="service"><a href="services.html">NOS SERVICES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<p style="position: relative; top: -100px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo nihil assumenda architecto, officia totam facere.</p>
				</div>


				<div class="dive3">
					<p class="service"><a href="contacter.html">CONTACTS</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					
				</div>



				<div class="dive4">
					<p class="service"><a href="#">PARTENAIRES</a></p>
					<hr style="position: relative; top: -100px; color: #666">
					<div class="partner">
						<div><a href=""><img style="height: 50px; width: 50px; top: 0px;" src="Images/HP_logo_2012.png" alt=""></a></div>
						<div><a href=""><img style="height: 45px; width: 55px; top: 0px;" src="Images/Dell_Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 15px;" src="Images/Canon_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Lexmark-Logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/microsoft_PNG16.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 30px;" src="Images/Cisco_logo.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/D-Link.png" alt=""></a></div>
						<div><a href=""><img style="height: 30px; width: 70px; top: 60px;" src="Images/Lenovo-logo-2015.png" alt=""></a></div>
					</div>
				</div>	
			</footer>
		</body>
		<script type="text/javascript" src="Styles/CodeJS.js"></script>
	</html>

	<script type="text/javascript">

		// le code du premier menu c est a dire menu
		var menu = document.getElementById('contMenu');
					//diapora = document.getElementById('diapora');
		window.onscroll = function(){
			if(window.pageYOffset > 700){
				menu.style.position = "fixed";
				menu.style.top = 0;
				menu.style.transition = .8;
			}
			else{
				menu.style.position = "relative";
				diapora.style.top = 50;
			};
		};

		//   le code du deuxieme menu c est dire le menu 2
		$(window).scroll(function() {
			if ($(document).scrollTop() > 700) {
				$('.contMenu').addClass('Menu2');
				$('.Mediatec').addClass('Mediatec2');
				$('.logo').addClass('logo2');
				$('.menu').addClass('menu2');
				$('.sInscrire').addClass('sInscrire2');
				$('.seConnecter').addClass('seConnecter2');
			}
			else{
				$('.contMenu').removeClass('Menu2');
				$('.Mediatec').removeClass('Mediatec2');
				$('.logo').removeClass('logo2');
				$('.menu').removeClass('menu2');
				$('.sInscrire').removeClass('sInscrire2');
				$('.seConnecter').removeClass('seConnecter2');
			}
		});

		
	</script>