@extends('layout.layout')

@section('content')
    <!DOCTYPE html>

    <script src="https://cdn.tiny.cloud/1/xkjdez2rca2u2kbmfharflo6vw46is1fy5usetd5jaacgtof/tinymce/5/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Envoyer Message</h6>
            <a href="sav.create"  class="btn btn-primary float-right">Nouveau</a>
        </div>

        @if($errors->any())


            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                @foreach($errors->all() as $error)


                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <li>
                        {!!$error!!}
                    </li>
                @endforeach
            </div>
        @endif
        @if($message = Session::get('success'))
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <li>
                {{$message}}
            </li>
            @endif
        <div class="card-body">
            <form action="send" method="post">
                    @csrf
                <div class="form-group">
                    <label for="" class="control-label">
                        Objet
                    </label>
                    <input type="text" name="objet" class="form-control">
                </div>

                <div class="form-group">
                    <label for="" class="control-label">
                        Selection les Mails
                    </label>
                    <select class="form-control" name="mail">
                        <option>gnakaleroland@gmail.com</option>
                    </select>
                </div>
                <label class="control-label" for="">
                    Message
                </label>
                <div class="form-group">

                
                    <textarea class="form-control" name="message"> </textarea>
                </div>

                <input type="submit" class="btn btn-danger" value="send">
            </form>
        </div>

@stop
