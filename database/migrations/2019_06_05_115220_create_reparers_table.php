<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReparersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Designation');
            $table->string('numserie');
            $table->date('date');
            $table->text('Methode');
            $table->string('Nomclient');
            $table->string('ContactClient');
            $table->string('NomTech');
            $table->string('ContactTech');
            $table->text('Motif');
            $table->timestamps();
        });
    }

    /**
      Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparers');
    }
}
