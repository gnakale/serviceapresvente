<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageSoumieController extends Controller
{


    public function index(){
        if(auth()->guest()){

            abort(403);
        }
        return view('MessageSoumie');
    }

    public function mail(){

        return view('MessageMail');
    }
}
