<html>

<head>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"><title>

    </title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>
<!--
    Realised by Thibault Leveau
    https://www.linkedin.com/in/thibault-leveau-a76923ba/
-->
<style>
    body {
        background-color: #e9ebee;
    }

    .card {
        margin-top: 1em;
    }

    /* IMG displaying */
    .person-card {
        margin-top: 5em;
        padding-top: 5em;
    }
    .person-card .card-title{
        text-align: center;
    }
    .person-card .person-img{
        width: 10em;
        position: absolute;
        top: -5em;
        left: 50%;
        margin-left: -5em;
        border-radius: 100%;
        overflow: hidden;
        background-color: white;
    }
</style>
<body>
<div class="container" style="margin-top: 1em;">
    <!-- Sign up form -->
    <form class="form-group" method="POST" action="/inscription">
    @csrf
    <!-- Sign up card -->
        @if($errors->any())


        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            @foreach($errors->all() as $error)


            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <li>
                {!!$error!!}
            </li>
            @endforeach
        </div>
        @endif
        <div class="card person-card">
            <div class="card-body">
                <!-- Sex image -->
                <img id="img_sex" class="person-img"
                     src="https://visualpharm.com/assets/217/Life%20Cycle-595b40b75ba036ed117d9ef0.svg">
                <h2 id="who_message" class="card-title"></h2>
                <!-- First row (on medium screen) -->
                <div class="row">
                    <div class="form-group col-md-2">
                        <select id="input_sex" name="sexe" class="form-control">
                            <option value="Mr.">Monsieur</option>
                            <option value="Ms.">Madame </option>
                        </select>
                    </div>
                    <div class="form-group col-md-5">
                        <input id="first_name" name="name" type="text" class="form-control" placeholder="Prénom">
                        <div id="first_name_feedback" class="invalid-feedback">

                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <input id="last_name" name="prenom" type="text" class="form-control" placeholder="Nom de famille">
                        <div id="last_name_feedback" class="invalid-feedback">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" style="padding=0.5em;">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Comment vous contacter ?</h2>
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="example@gmail.com">
                            <div class="email-feedback">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tel" class="col-form-label">Phone number</label>
                            <input type="text" name="phone" class="form-control" id="tel" placeholder="+225 67 50 96 22">
                            <div class="phone-feedback">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Sécurisez votre compte !</h2>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Mot de Passe</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="Type your password" >
                            <div class="password-feedback">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password_conf" class="col-form-label">Mot de Passe (confirm)</label>
                            <input type="password" name="password_confirmation" class="form-control" id="password_conf" placeholder="Type your password again">
                            <div class="password_conf-feedback">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 1em;">
            <button type="submit" class="btn btn-primary btn-lg btn-block">S'inscrire !</button>
        </div>
    </form>
</div>
<script>
    // URLs images
    var female_img = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSo8PcCxh7tT6MDFhJi2UaAT9SeciHqvZuaWtGg0y0-yyP8rMDz";
    var male_img = "https://visualpharm.com/assets/217/Life%20Cycle-595b40b75ba036ed117d9ef0.svg";

    // On page loaded
    $( document ).ready(function() {
        // Set the sex image
        set_sex_img();

        // Set the "who" message
        set_who_message();

        // On change sex input
        $("#input_sex").change(function() {
            // Set the sex image
            set_sex_img();
            set_who_message();
        });

        // On change fist name input
        $("#first_name").keyup(function() {
            // Set the "who" message
            set_who_message();

            if(validation_name($("#first_name").val()).code == 0) {
                $("#first_name").attr("class", "form-control is-invalid");
                $("#first_name_feedback").html(validation_name($("#first_name").val()).message);
            } else {
                $("#first_name").attr("class", "form-control");
            }
        });

        // On change last name input
        $("#last_name").keyup(function() {
            // Set the "who" message
            set_who_message();

            if(validation_name($("#last_name").val()).code == 0) {
                $("#last_name").attr("class", "form-control is-invalid");
                $("#last_name_feedback").html(validation_name($("#last_name").val()).message);
            } else {
                $("#last_name").attr("class", "form-control");
            }
        });
    });

    /**
     *   Set image path (Mr. or Ms.)
     */
    function set_sex_img() {
        var sex = $("#input_sex").val();
        if (sex == "Mr.") {
            // male
            $("#img_sex").attr("src", male_img);
        } else {
            // female
            $("#img_sex").attr("src", female_img);
        }
    }

    /**
     *   Set "who" message
     */
    function set_who_message() {
        var sex = $("#input_sex").val();
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();

        if (validation_name(first_name).code == 0 ||
            validation_name(last_name).code == 0) {
            // Informations not completed
            $("#who_message").html("Qui êtes-vous?");
        } else {
            // Informations completed
            $("#who_message").html(sex+" "+first_name+" "+last_name);
        }
    }

    /**
     *   Validation function for last name and first name
     */
    function validation_name (val) {
        if (val.length < 2) {
            // is not valid : name length
            return {"code":0, "message":"Le nom est trop court"};
        }
        if (!val.match("^[a-zA-Z\- ]+$")) {
            // is not valid : bad character
            return {"code":0, "message":"le"};
        }

        // is valid
        return {"code": 1};
    }
</script>
</body>
</html>
