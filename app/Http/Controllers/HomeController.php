<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Sav;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->guest()){

            abort(403);
        }
        $count = DB::table('savs')->count();
        $reserve = DB::table('reserves')->count();
        $reparer = DB::table('reparers')->count();

        return view('admin-sav',compact('count','reserve','reparer'));
    }
}
