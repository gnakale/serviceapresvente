<?php

namespace App\Http\Controllers;

use App\Reparer;
use Illuminate\Http\Request;

class ReparationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {



        $this->middleware(['auth','confirmed','Admin'])->except(['show','store','create','index']);


    }

    public function index()
    {
        if(auth()->guest()){

            abort(403);
        }
        $reparer = Reparer::all();

        return view('reparer',[

            'reparer' => $reparer
        ]);

    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->guest()){

            abort(403);
        }
        return  view('reparerCreate');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if(auth()->guest()){

            abort(403);
        }
        Request()->validate([

            'Designation'=>'required|min:8',
            'numserie'=>'required',
            'date'=>'required|date',
            'Motif'=>'required',
            'Methode'=>'required',
            'Nomclient'=>'required|min:8',
            'ContactClient'=>'required',
            'NomTech'=>'required',
            'ContactTech'=>'required',
            ''

        ]);


        Reparer::create([
            'Designation'=>\request('Designation'),
            'numserie'=>\request('numserie'),
            'date'=>\request('date'),
            'Motif'=>\request('Motif'),
            'Methode'=>\request('Methode'),
            'Nomclient'=>\request('Nomclient'),
            'ContactClient'=>\request('ContactClient'),
            'NomTech'=>\request('NomTech'),
            'ContactTech'=>\request('ContactTech')

        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->guest()){

            abort(403);
        }
        $reparer = Reparer::where('id',$id)->first();

        return view('reparer-show',[

            'reparer'=>$reparer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
