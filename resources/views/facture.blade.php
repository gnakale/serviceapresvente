<!doctype html>
	<html lang="fr">
	<head>
	
		<style type="text/css">
			header{
				padding: 0px;
				margin: 0px;
			}
			.logo{
				position: absolute;
				font-size: 14px;
			}
			.logo ul li{
				margin-top: 10px;
				margin-left: -30px;
			}
			.logo img{
				height: 60px;
				width: 200px;
				margin-left: -10px;
			}
			.SAV{
				position: absolute;
				left: 240px;
				top: 32px;
				font-size: 20px;
				text-transform: uppercase;
				color: red;
			}
			.date{
				position: absolute;
				left: 490px;
				margin-top: 10px;
			}
			.date p{
				font-size: 18px;
				line-height: 30px;
			}
			.bon{
				position: absolute;
				top: 165px;
			}
			.bon b{
				color: red;
			}
			.bon h1{
				text-align: center;
			}
			.corps{
				position: absolute;
				top: 270px;
			}
			.corps th{
				border: solid 1px #666;
				text-align: center;
				height: 35px;
			}
			.area td{
				border: solid 1px #666;
				height: 220px;
			}
			.frais th{
				border: solid 1px #666;
				height: 35px;
			}
			.avertir{
				position: absolute;
				top: 590px;
			}
			.avertir p{
				line-height: 8px;
				font-size: 13px;
				font-weight: 200;
			}
			.info-clt, .info-tech{
				position: absolute;
				top: 690px;
			}
			.info-clt th, .info-tech th{
				border: solid 1px #666;
				height: 30px;
				text-align: center;
			}
			.info-clt td, .info-tech td{
				border: solid 1px #666;
				height: 100px;
			}
			.info-clt table{
				width: 48%;
			}
			.info-clt p, .info-tech p{
				padding: 0px 40px;
			}
			.info-tech table{
				width: 345px;
			}
			.info-tech{
				left: 360px;
			}
			.footer {
				position: absolute;
				top: 950px;
				margin: 0px -70px;
			}
			.footer p{
				line-height: 8px;
				font-size: 13px;
				font-weight: 200;
				text-align: center;
			}
		</style>
	</head>
		<body>
	
			<header class="header">
				<div class="logo">
					<img src="images/mediatec.PNG" alt="">
					<ul>
						<li>SERVICES INFORMATIQUES</li>
						<li>BUREAUTIQUES</li>
						<li>TRAVAUX DIVERS</li>
					</ul>
				</div>

				<div class="SAV">
					<input type="radio" name="sav" checked> <b>{{$facture->option}}</b><br><br>
				</div>

				<div class="date">
					<p>Date d'entrée : <b>{{$facture->DateSav}}</b></p>
				</div>

				<div class="bon">
					<h1>BON DE RECEPTION N° : <b>MDTC0001</b> </h1>
				</div>
			</header>
			

			<div class="corps">
				<table style="width: 100%;" cellspacing="0">
					<tr>
						<th width="10%">Quantité</th>
						<th width="45%">Désignation</th>
						<th width="25%">Motif</th>
						<th width="20%">N° de Série</th>
					</tr>
					<tr class="area">
						<td style="text-align: center; font-size: 16px"><b>{{$facture->qte}}</b></td>
						<td>{{$facture->designation}}</td>
						<td>{{$facture->motif}}</td>
						<td>{{$facture->serie}}</td>
					</tr>
					<tr class="frais">
						<td></td>
						<td></td>
						<th>Frais de Diagnostique </th>
						<th></th>
					</tr>
				</table>
			</div>


			<div class="avertir">
				<p>Après délai de récupération (7 Jours ouvrables à compter de l'appel du service technique ) non respecté par le Client l'équipement fera</p>
				<p>l'objet de frais de magasinage et MEDIATEC se désengage de toute responsabilité des événtuels problèmes survenus après cette date.</p>
			</div>


			<div class="info-clt">
				<table cellspacing="0">
					<tr>
						<th colspan="3">INFORMATIONS DU CLIENT</th>
					</tr>
					<tr>
						<td colspan="3">
							<p>Nom : <b>{{$facture->nom_client}}</b> </p>
							<p>Contact : <b>{{$facture->contact_client}}</b> </p>
							<p>E-mail : <b>{{$facture->email_client}}</b> </p>
						</td>
					</tr>
				</table>
			</div>


			<div class="info-tech">
				<table cellspacing="0">
					<tr>
						<th colspan="3">INFORMATIONS DU TECHNICIEN</th>
					</tr>
					<tr>
						<td colspan="3">
							<p>Nom : <b>{{$facture->nom_tech}}</b> </p>
							<p>Contact : <b>{{$facture->contact_tech}}</b> </p>
							<p>E-mail : <b>{{$facture->email_tech}}</b> </p>
						</td>
					</tr>
				</table>
			</div>


			<h2 style="position: absolute; top: 850px; font-size: 12px;">NOUS VOUS REVENONS DANS 72 HEURES MAXIMUM.</h2>

            <hr style="position: relative; top: 910px;">


			<div class="footer">
				<p>Abidjan Plateau Av. Franchet d'Esperey Im. Abeilles -17 BP 1316 Abidjan 17 -Tel:<b> 20 21 88 44</b> Fax: <b>67 50 96 22</b> </p>
				<p>C.C.N°1224195P -R.C.N°CI-ABJ-2011-B2020 -Compte Bancaire BNI 00269240004 BNI / <b>www.mediatec-ci.com</b> / <b>infos@mediatec-ci.com</b></p>
			</div>
		</body>
	</html>



